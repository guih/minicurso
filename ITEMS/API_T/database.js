var mysql = require('mysql');

var db = mysql.createPool({
	user:"root",
	password: "root",
	host: "localhost",
	database: "aula"
});

exports.insertItem = function(data, fn){
	db.getConnection(function(error, connection){
		var sql = "INSERT INTO notas set ?";

		connection.query(sql, data, function(error, row){
			if(error) {
				console.log('Error! Cant insert.', error);
				return false;
			}

			if( row.affectedRows ){
				fn(true);
				return false;
			}

			fn(false);
		});
	});
};
exports.updateItem = function(data, id, fn){
	db.getConnection(function(error, connection){
		
		var sql = "UPDATE notas set ? WHERE id = ?;";

		connection.query(sql, [data, id], function(error, row){
			if( error ){
				console.log('Error! Cant edit.');
				return;
			}

			if( row.affectedRows ){
				fn(true);
				return false;
			}

			fn(false);
		});
	});
};

exports.deleteItem = function(id, fn){
	db.getConnection(function(error, connection){
		var sql = "DELETE FROM notas WHERE id = ?;";

		connection.query(sql, id, function(error, row){
			if( error ){
				console.log('Error! Cant delete.');
				return false;
			}

			if( row.affectedRows ){
				fn(true);
				return false;
			}

			fn( false );

		});
	});
};


exports.getItems = function(fn){
	db.getConnection(function(error, connection){
		var sql = "SELECT * FROM notas WHERE 1;"
		connection.query(sql, null, function(error, rows){
			if( error ){
				console.log('Error! Cant select!');
				return false;
			}

			if( Object.keys(rows).length > 0 ){
				fn(rows);
				connection.release();
				return false;
			}else{
				fn(false);
			}
		});
	});
};