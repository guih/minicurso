exports.conver2JSON = function(data, fn){
	var str = data.split("&");
	var obj = {};

	for( var i = 0; i< str.length; i++ ){
		var chunk = str[i].split("=");

		obj[chunk[0]] = decodeURIComponent(chunk[1].replace(/\+/g,  " "));
	}

	fn(obj);
};

//title=TEXT&text=TEXT%0D%0A