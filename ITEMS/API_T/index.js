var http = require('http');
var _router = require('router');
var utils = require('./utils');
var db  = require('./database');
http.globalAgent.maxSockets = 1000000;

var router = _router();

var server = function(req, res){
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Accept, null');
	router(req, res, function(){});
}

router.get('/', function(req, res){
	res.writeHead(200, {"Content-Type": "text/plain"});
	res.end("Hello world!");
});

router.get('/items', function(req, res){
	console.log('TRY');
	db.getItems(function(items){
		console.log(items);
		if( items ){
			var myEnd = JSON.stringify(items)
			res.end(myEnd);
		}else{
			var endError = {
				error: true,
				info : "Algo está errado!"
			}
			res.end(JSON.stringify(endError));
		}
	});
});


router.get('/delete/:id', function(req, res){
	var id = req.params.id;

	if( !isNaN(id) ){
		db.deleteItem(id, function(resp){
			if( resp == true ){
				var myEnd = {
					info : "Excluido com sucesso!",
				}

				res.end(JSON.stringify(myEnd));
			}else{
				var endError = {
					error: true,
					info : "Algo está errado!"
				};
				res.end(JSON.stringify(endError));
			}
		});	
	}else{
		var endError = {
			error: true,
			info: "Id inválido, tente novamente"
		};

		res.end(JSON.stringify(endError));
	}
});

router.post('/add', function(req, res){
	var body = "";

	req.on('data', function(data){
		body += data;
	});

	req.on('end', function(){
		utils.conver2JSON(body, function(json){
			var toInsert = {
				title: json.title,
				content: json.text,
				timex: ( +new Date() )
			};
			db.insertItem(toInsert, function(resp){
				if( resp == true ){
					var myEnd = {
						info: "Inserido com sucesso!"
					};
					res.end(JSON.stringify(myEnd));
				}
			});
		});
	});	
});

router.post('/edit', function(req, res){
	
	var body = "";
	req.on('data', function(data){
		body += data;
	});

	req.on('end', function(){
		utils.conver2JSON(body, function(json){
			var toEdit = {
				title: json.title,
				content: json.text,
				timex: (+new Date())
			};
			var id = json.id;
			db.updateItem(toEdit, id, function(resp){
				if( resp == true ){
					var myEnd = {
						info: "Editado com sucesso!"
					};

					res.end(JSON.stringify(myEnd));
				}else{
					var endError = {
						erro: true,
						info: "Algo esta errado!"
					};
					res.end(JSON.stringify(endError));

				}
			});
		});
	});

	// res.end();
});



router.get('*', function(req, res){
	res.writeHead(404, {"Content-Type": "text/plain"});
	res.end("404 not found!")
});

http.createServer(server).listen(1337);