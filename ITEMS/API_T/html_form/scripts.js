function ajaxGet(url, fn){
	var ajax = new XMLHttpRequest();

	ajax.onreadystatechange = function(){
        if (ajax.readyState == 4 && ajax.status == 200){
            fn(ajax.responseText);
        }
	}
	ajax.open("GET", url, true);
	ajax.send();
}
 
function updateInterface(){
	var tableBody = document.getElementsByTagName('tbody')[0];

	var markup = "";
	ajaxGet('http://localhost:1337/items', function(items){
		var items = JSON.parse(items);


		for( var i = 0; i< items.length; i++ ){
			markup += "<tr>";		
			markup += "	<td>"+items[i].id+"</td>";		
			markup += "	<td>"+items[i].title+"</td>";
			markup += "	<td>"+items[i].content+"</td>";

			markup += "	<td>"+items[i].timex+"</td>";
			markup += "</tr>";	
		}
		tableBody.innerHTML = "";
		tableBody.innerHTML = markup;
	});

}

updateInterface();