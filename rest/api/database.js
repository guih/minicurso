var mysql = require('mysql');

var db = mysql.createPool({
	user: 'root',
	password: '',
	host: 'localhost',
	database: 'palestra_01'
});


exports.insertItem = function(data, fn){
	db.getConnection(function(error, connection){

		var sql = "INSERT INTO notas set ?;";
		
		connection.query(sql, data, function(error, row){
			if( error ) console.log(error);

			if( row.affectedRows == "1" ){
				fn({error: false, lastInsert: row.insertId});
			}else{
				fn({error: true});
			}
		});
	});
};