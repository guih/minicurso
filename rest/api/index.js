var http = require('http');
var Router = require('router');
var util = require('./utils');



/* Instancia do objeto "router" */
var router = Router();


router.get('/', function(req, res){
	res.writeHead(200, {"Content-Type": "text/plain"});
	res.end('Hello World! This is our REST API.');
});

router.get('/notas', function(req, res){
	res.writeHead(200, {'Content-Type': 'application/json'});
	res.end('GET /notas');
});

router.get('/nota/:id', function(req, res){
	res.writeHead(200, {"Content-Type": "application/json"});
	var id = req.params.id;
	res.end("GET /nota/"+id); 
});

router.get('/nota/:id/delete', function(req, res){
	res.writeHead(200, {"Content-Type": "application/json"});
	var id = req.params.id;
	res.end("GET /nota/"+id+"/delete");
});

router.post('/add', function(req, res){
    var body = '';
    req.on('data', function (chunk) {
        body += chunk;
    });
    req.on('end', function () {
        res.writeHead(200, {'Content-Type': 'application/json'});
        util.convertFormData(body, function(re){
        	re.timex = Math.round( +new Date() / 1000 );
        	res.end("POST /add/\n\n"+JSON.stringify(re));
        });
    });


	//res.end("POST /nota/add/" + JSON.stringify(nota));
});

router.use('/save', function(req, res){
	res.writeHead(200, {"Content-Type": "application/json"});
	var nota = req.body.data;
	res.end("POST /nota/save/" + JSON.stringify(nota));
});


 /*  404 controll */
router.get('*', function(req,res){
	res.writeHead(404, {"Content-Type": "text/plain"});
	res.end("404");
});

/* Server configuration */
var server = function(req, res){
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Accept');
	router(req, res, function(){});
}


/* Start server */
http.createServer(server).listen(1999);


console.log('Listening at http://localhost:1999');